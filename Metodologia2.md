Unidad 2
========================================

Hipótesis de mi Trabajo
----------------------------------------
Es posible establecer una relación *co-algebraica* de costos entre programas de carácter general
que permita esbozar técnicas generales para probar optimizaciones en un amplio
espectro de lenguajes que acepten una descripción *algebraica*.

Forma: General. Dado que se platean supuestos generales del objetivo concreto de
la investigación que es dar evidencia que sí se puede construir dicha relación
para ciertos lenguajes.

Tipo: Hipotesis Descriptiva: Proponen explicar que debido que cierto lenguaje L
acepta una descripción algebraica permite hacer análisis intencional sobre su
ejecución y en particular hacer análisis de costos. Es decir que se busca
esbozar características sobre lenguajes con el supuesto que posean cierta
estructura algebraica. El objetivo de la Tesis es entonces demostrar que así lo
es, esbozando cómo es posible hacerlo.

Análisis de Variables
----------------------------------------
Una variable, existencia de relación co-algebraica de costos entre programas
escritos en lenguajes algebraicos. Variable cualitativa nominal ya que expresa
una categoría del objeto de estudio en cuestión y habla sobre la existencia de
cierta propiedad y no expresa orden alguno.

+ Variable conceptual: Sea L un lenguaje algebraico que acepta una evaluación parcial *ev : L -> L *,
decimos que M es mejorado por N si para todo contexto C, C[M] y C[N] son construcciones correctas en L,
si ev(C[M]) termina en k-pasos entonces ev(C[N]) termina en a lo sumo k-pasos.

+ Variable Operacional: Presentar la evaluación como una construcción monótona sobre el
*Lattice completo de Conjuntos ordenados por contención*, dicha construcción
acepta una simulación que eventualmente permite probar optimizaciones sobre la
relación presentada de forma conceptual.

Objetivos de Tesis
----------------------------------------
El tema de mi tesis fue evolucionando y cambiando a lo largo del estudio
concreto de la misma. En particular se probo al principio que la tesis
presentada para la beca era *FALSA*, y luego continué por otro camino.
Transcribir dichos objetivos me parece que no servirían en este momento.

Por lo que en este caso escribiré los objetivos que creo que fueron realmente atacados:

Objetivo General: Dar una construcción de Mejoras de Programas para lenguajes de
programación de forma abstracta que acepten una caraterización algebraica.
Objetivos Específicos:

- Caracterizar los Lenguajes de Programación que aceptan una descripción
  algebraica.
  Responde al a la pregunta "Qué hay?" del tipo *perceptual* con el objetivo de
  explorar lo existente en el área presentando la base descriptiva del trabajo a
  realizar.
- Presentar una relación de mejoras de programas de forma CoAlgebraica.
  Objetivo concreto de la tesis, de tipo *comprensivo* que consiste en
  *proponer* una solución al objetivo general.
- Comparar dichas relaciones obtenidas con las caracterizaciones tradicionales
  de teoría de mejoras para lenguajes específicos.
  De tipo *integrativo* que permite *confirmar* que la solución propuesta es realmente una solución.
- Mostrar la aplicabilidad de dicha caracterización general para mostrar
  optimizaciones para un amplio espectro de lenguajes de programación de forma
  abstracta. De tipo *integrativo* que permite *evaluar* la solución con el
  objetivo de responder a la preguntar *¿Qué tan efectivo es?*.

Observaciones
----------------------------------------

Quisiera aclarar que la mayoría del contenido fue desarrollado específicamente
para este documento. Lamentablemente el desarrollo de mi investigación no fue
para nada preciso, riguroso, científico, ni premeditado. La propuesta de tesis
original, que mi director corrigió pero que me pidió escribir a mí, pudimos dar
una respuesta negativa. Luego proseguimos investigando de forma más bien
aleatoria, con poca comunicación Director/Estudiante, hasta que pude encontrar
un problema y resolverlo, que espero sea suficiente para la obtención del titulo
de doctorado.
