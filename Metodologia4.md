Síntesis de proyecto de tesis.
========================================

Los primeros tres puntos fueron copiados de la entrega anterior.

+ Problema de Investigación / Hipótesis: 
Es posible establecer una relación *co-algebraica* de costos entre programas de carácter general
que permita esbozar técnicas generales para probar optimizaciones en un amplio
espectro de lenguajes que acepten una descripción *algebraica*.

+ Objetivos:
La construcción de un entramado conceptual matemático que permita la elaboración
directa de pruebas de optimizaciones de la forma más simple posible.
Se espera de esta manera avanzar en el conocimiento de las técnicas utilizadas
como a su vez desarrollar nuevas. Presentar un estudio más completo sobre los
tipos de lenguajes sobre el cual dicha técnica es aplicable.

+ Estrategias metodológicas: Estudio de Tipo *explorativo*. Se encuentra
bibliografía anterior a éste trabajo, pero se aborda desde otro punto de vista
desconectado de otras teorías.

----------------------------------------

+ Instrumentos/Técnicas/Métodos:
Debido a la estrategia metodológica utilizada, y al tipo de investigación que se
realiza, las técnicas utilizadas son de tipo *cuantitativo**.

A su vez, al ser un estudio de tipo *explorativo* se utiliza la técnica de
*experimentación*. En mi caso particular, la noción de experimentación no está
tan relacionada con la experimentación física (o química), sino más bien con una
connotación matemática. Mi objetivo es la construcción de un entramado
conceptual matemático, y por ende se plantea una estructura matemática que
provea sentido semántico y mecanismos de manipulación de estos sentidos que
representen nociones de costos sobre programas.

+ Medición: Una vez obtenido *un entramado conceptual* se busca *medir*, o más
  certeramente mostrar que el sistema es *consistente*. Es decir que no es
  posible explicar con él *fenómenos* incorrectos, o que contradigan la
  *realidad*. Concretamente, que no podamos encontrar dos programas P,Q, donde P
  sea más rápido que Q cuando lo corremos en una computadora *real*, y que
  cuando los interpretemos dentro del sistema, la *interpretación de Q* sea más
  *rápida* que la *interpretación de P*.
  
  Además de ser consistentes se busca explorar *que tan expresivo es el
  entramado*, es decir, se buscan aplicaciones conocidas a entramados similares
  y se busca aplicar de la misma forma.
  
  Por último, se mide a su vez la *elegancia matemática*. En palabras
  científicas, se utiliza una heurística para explorar diferentes entramados
  conceptuales, *la intuición.*

+ Variables e Indicadores: Las mediciones son correctas o incorrectas por lo que
  no estoy seguro que se pueda realizar este tipo de análisis. Aunque podríamos
  *agrupar* entramados correctos de forma *Nominal*, aquellos que son
  consistentes.

+ Comentarios/Observaciones:

Cambié el punto de **Unidad de Análisis** por **Medición**, termino que me
pareció más adecuado.

----------------------------------------

Retomando los puntos anteriores, el desarrollo de mi investigación la considero
de tipo exploratorio/experimental, por lo que desarrollo el siguiente enunciado.

La elaboración de uno de los instrumentos para la recolección de datos de la
investigación de tesis mencionado en la tabla síntesis, para investigaciones
empíricas: Concretamente para estudiar y medir el desarrollo de una *Teoría de
Mejoras*, se suelen comparar contra otras teorías o resultados conocidos en el
área. Un ejemplo es una optimización concreta de lenguajes de programación
conocida como *Common SubExpression Elimination* (Eliminación de SubExpresiones
Comunes), donde se busca identificar fragmentos de programas que se repitan para
*ejecutarlos* una vez y compartir el resultado. En general las diferentes formas
de *Teoría de Mejoras* nacen a partir de una característica fundamental del
lenguaje que se esté estudiando en ése momento, presentando la *habilidad* de
**poder** realizar dicho análisis en ése lenguaje. Actualmente me encuentro
estudiando una forma más abstracta que pueda conectar las diferentes nociones
conocidas, por lo que el instrumento y método de medición es poder *derivar* de
mí marco teórico las diferentes teorías conocidas al momento de analizar
distintos lenguajes.
