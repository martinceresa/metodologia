Buenas!

Mi camino por el doctorado me ha llevado a ir cambiando bastante las
motivaciones por las cuales fui eligiendo los caminos y los temas, terminando en
lo que es mí tesis hoy en día.

Al comenzar el doctorado, tal cual lo plantea mi propuesta doctoral, se basaba
en una tesis en una área que hoy en día ya no es mi interés. En retrospectiva
tampoco lo era en ese momento, lo que me interesaba particularmente era trabajar
con mí director y las técnicas que se planteaban a utilizar que nunca se
llegaron a usar. Particularmente porque luego de trabajar y desarrollar el tema
durante los primeros dos años (claramente no muy felices) llegué a mostrar que
no se podía resolver dicho problema, trabajo que intentamos publicar pero la
comunidad no se toma muy bien los resultados negativos y terminó encajonado por
ahí.

Aplicando la definición dada por el plan de estudios del *Doctorado en
Informática* de FCEIA, el objetivo es capacitar a los doctorandos para la
**investigación y desarrollo** dentro de la informática que impliquen avances
*importantes y originales*. Mí plan plantea el estudio de la computación
paralela, es decir, estudiar cómo se ejecutan los programas en computadoras con
múltiples núcleos con el objetivo de mejorar la velocidad del programa. Hoy en
día mi tesis se basa en la optimización de programas, no necesariamente
paralelos.

La optimización de programas podemos definirla rápidamente
como una transformación de un programa *P* a uno *Q* de manera tal que ambos
hagan lo mismo, se comporten igual, pero que *Q* sea más rápido que *P**. Y el
área de investigación de optimización de programas está en pleno desarrollo
dentro de las ciencias más aplicada de la computación.

Sin embargo, el desarrollo de la tesis se centra en una investigación básica, es
decir, *sin pensar en darles ninguna aplicación o utilización determinada*. Se
busca caracterizar diversos lenguajes de programación de forma genérica buscando
un marco teórico general donde se puedan *expresar* y *probar* optimizaciones de
programas, sin depender de características particulares de cada lenguaje de
programación o de llegado al caso plantearlas de forma general, con el objetivo
de poder trasladar dichas optimizaciones a lenguajes que compartan dichas
características.

Lamentablemente este problema está muy lejos de ser relacionado con problemas
dentro del marco de las políticas de ciencia y tecnología planteadas por el Plan
Nacional de Ciencia, Tecnología e Innovación. Ya que en general las políticas
empujan a un desarrollo más social, o que eventualmente mejoren la calidad de
vida o economía de la región. Sin embargo, se puede buscar una pequeña conexión
con el objetivo de optimizar programas buscando reducir el consumo energético
de los mismos, aunque no es el objetivo fundamental de la tesis.

Como conclusión debo decir que mi investigación está un poco fuera de los
objetivos que se marcan dentro del Plan Nacional como del propuesto por Sábato y
Botana, que plantean una mirada mucho más social y cultural de la investigación.
Me gustaría creer que en algún momento mis investigación puede llegar a ser
relevante en el ámbito de la industria del desarrollo del software, pero
sinceramente no creo ni que sea relevante para la comunidad científica.
