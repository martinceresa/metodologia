\documentclass[12pt,a4paper]{article}

\usepackage[backend=biber]{biblatex}
\bibliography{cites} 

%%%%%%%%%%%%%%%%%%%%
\usepackage{fontspec}
\setmainfont{Times New Roman}
%%%%%%%%%%%%%%%%%%%%
\usepackage{geometry}
\geometry{lmargin=3cm, rmargin=2.5cm}
%%%%%%%%%%%%%%%%%%%%
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}

\newtheorem{definition}{Definición}

\newcommand\mkRel[3]{{#1} \, {#2} \, {#3}}
\newcommand\tick[1][]{\checkmark^{#1}}
\newcommand\tickt{\checkmark^T}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Top Matter
\title{\textbf{Mejoras de Programas con Efectos}}
\author{Ceresa, Martín Arnaldo
  \\ National University of Rosario, Argentina
  \\ CIFASIS - CONICET%
}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
  Optimizar programas es difícil. No sólo se debe preservar el significado de un
programa sino que además se debe asegurar que la optimización realmente mejore
el programa. Por un lado preservar el significado de los programas es, hasta el
día de hoy, un campo en desarrollo. En este trabajo seguiremos una linea de
trabajo que comienza con la equivalencia observacional de \emph{Morris}, seguido
por la noción de bisimilaridad de \emph{Abramsky}, y el método de \emph{Howe},
que concluye con una formalización abstracta teniendo en cuenta efectos
algebraicos presentada por \emph{Dal Lago, Gavazzo y Levy}. Mientras que
asegurarnos que una optimización realmente mejore un programa es camino menos
transitado, presentando a la teoría de \emph{Sands} como su mayor exponente. En
éste trabajo conectaremos ambas partes obteniendo una \emph{Teoría de Mejoras}
abstracta basada en una noción de bisimilaridad en presencia de efectos
computacionales que extiende la noción de \emph{Sands} a lenguajes con efectos.
\end{abstract}

\section{Introducción}

Optimizar programas es una tarea difícil, ya que debemos tener en cuenta dos
aspectos diferentes de la computación:
\begin{itemize}
  \item Cómo computamos, y
  \item Qué computamos.
\end{itemize}

Al optimizar idealmente se busca mejorar \emph{cómo} se realiza una computación
\textbf{sin modificar} \emph{qué} estamos computando. Es decir, preservar el
sentido de nuestros programas, mientras mejoramos aspectos \emph{intencionales}
de la evaluación o ejecución de los mismos. Por lo que, nuestra tarea entonces,
es \emph{observar} propiedades intencionales de la ejecución, como ser
\emph{tiempo}, \emph{consumo de energía}, \emph{uso de memoria}, etc.

La \emph{Teoría de Mejoras}\cite{Sands:Op:Theories} presenta exactamente dicho
análisis para \emph{lenguajes funcionales puros} con divergencia. Presenta a su
vez una relación que denomina \emph{mejora} como un \emph{refinamiento} de la
noción de equivalencia de programas. Esto permite conectar la correcta
transformación de programas con una intencional sobre la ejecución de los mismos.

\subsection{De equivalencia a Mejoras}

\emph{Equivalencia Observacional}\cite{morrisphd} dice que dos programas son
equivalentes si no hay un contexto que los distinga. Asumiendo que tenemos una
relación que nos indica si un programa \(t\) termina, lo notamos con \(t
\downarrow\), podemos definirlo en símbolos como:
\[
  t \cong u \; \triangleq \; \forall \mathbb{C}, \; \mathbb{C}[t] \downarrow
  \iff \mathbb{C}[u] \downarrow
\]

Una prueba de equivalencia de programas, \(p\) y \(q\), incluye tener que probar
que \emph{para todo} contexto usar \(p\) o \(q\) es indiferente. Esto trae un
gran problema, probar para que para todo contexto efectivamente es lo mismo
cualquiera de los dos es muy difícil. Para subsanar el problema,
\emph{Abramsky}~\cite{Abramsky:1990:LLC:119830.119834} introduce un concepto
llamado \emph{bisimilaridad aplicativa}. Bisimilaridad aplicativa es una noción
más simple que la equivalencia observacional pero bajo ciertas condiciones nos
permite garantizar que bisimilaridad aplicativa implica de forma denotacional
equivalencia observacional. \emph{Howe} su vez, en la búsqueda de aplicar las
mismas técnicas pero de forma operacional~\cite{Howe:Cong:Bisim}, permitiendole
a \emph{Sands} introducir análisis intencionales sobre
programas~\cite{Sands:Op:Theories}

Un ejemplo sencillo puede ser: sea \(M \mapsto 1 + 1\) y \(N \mapsto 2\),
intuitivamente en cualquier contexto que nos encontremos con \(M\) podremos
intercambiarlo por \(N\).

Mejoras funciona de la misma manera que la equivalencia observacional, pero se
utilizan una relación de evaluación indexada por orden que representa el costo
de la evaluación. Por ejemplo, no sólo estaremos interesados en una relación de
terminación (\( t \downarrow \)), sino que además en una evaluación con costo
\(n\) (\(t \downarrow_n\)). \emph{Sands} comienza por definir la noción de
mejoras, que como equivalencia observacional, se define para todo contexto:
\begin{definition}[Mejora]
  Dado dos programas \(t\) y \(u\), se dice que \(t\) es mejorado por \(u\)
  cuando en cualquier contexto reemplazar \(t\) por \(u\) \textbf{no} incrementa
  el costo de la evaluación.
  \[
    t \trianglerighteq u \; \triangleq \forall \; \mathbb{C}, n \in \mathbb{N},
    \mathbb{C}[t]\downarrow_n \implies \exists \, m \in \mathbb{N},
    \mathbb{C}[u]\downarrow_m \; \land \; n \geq m
  \]
  \end{definition}

Retomando el ejemplo anterior, intuitivamente podemos ver que tener el valor \(2\)
directamente incluye menor costo que realizar la suma \(1 + 1\).

La definición de mejoras presenta las misma carencias que la definición de
equivalencia observacional de programas. Mostrar que un programa mejora o es
mejorado por otro incluye tener que mostrar que es así \textbf{para todo
  contexto posible}.

Para subsanar dicho problemas en las definiciones se utiliza un mecanismo
presentado por Abramsky~\cite{Abramsky:1990:LLC:119830.119834}
llamado \emph{simulación aplicativa}. La idea es conectar un concepto
\emph{global} representado por la idea de todo posible contexto, por una noción
más local que involucre la ejecución del programa. El mismo objetivo que llevo a
Sands a presentar lemas auxiliares.

Howe~\cite{Howe:Cong:Bisim,Howe:Eq} introduce un método que permite obtener
el mecanismo presentado por Abramsky partiendo la equivalencia observacional de
programas. A su vez, Sands redefine su teoría siguiendo este método obteniendo
una noción similar para su teoría de mejoras de programas~\cite{Sands:Op:Theories}.

Recientemente el método de Howe fue extendido~\cite{Eff:App:Bisim} para
lenguajes con efectos computaciones, donde los efectos son generados por
operaciones algebraicas y consumidos por mónadas.

Por ejemplo, dado los programas \(M_f \mapsto \text{print(``Hola'')}\; ; 1 + 1\) y
\(N_f \mapsto \text{print(``Hola'')}\; ; 2\), son equivalentes?. Intuitivamente
podemos pensar que al momento de evaluarlos ambos imprimen en pantalla el
mensaje ``Hola'' y luego presentan el mismo valor \(2\).

Siguiendo los pasos de Sands en este trabajo buscaremos extender la teoría de
mejoras para a su vez podes manipular efectos computaciones. Notar que la
ausencia de efectos es un efecto, y por ende deberíamos ser capaces de capturar
el trabajo propuesto por Sands.

Siguiendo el ejemplo nos preguntamos si podremos mostrar que el programa con
efectos \(N_f\) es mejor que el programa con efectos \(M_f\)
  
En las siguientes secciones presentaremos el objetivo de éste desarrollo, y cómo
llevarlo a cabo. 

\section{Perspectiva Téorica}

Partiendo de la definición de equivalencia observacional y aproximación
observacional se plantea la definición de una teoría de mejoras permitiendo el
uso de efectos computacionales. Siguiendo la idea intuitiva de \emph{mejora como
noción refinada de equivalencia o aproximación observación}, se plantea la
posibilidad de refinar las nociones resultantes utilizando el concepto de
\emph{Transformador Monadico de Escritura}.

El objetivo es presentar una teoría de mejoras que permita realizar análisis
intencional sobre programas con efectos computacionales. Para esto nos basaremos
en el trabajo desarrollado en \cite{Eff:App:Bisim} interpretando el costo de la
evaluación de un programa como un efecto adicional que podremos observar
directamente.

En la próxima sección podremos ver como construir una definición observacional y
derivar una definición concreta de teoría de mejoras. A su vez, mediante el
método de Howe, y su extensión presentada por Dal Lago, se dará una forma simple
que evita las pruebas universales sobre contextos.

\section{Metodología}

En la presente sección introduciremos los conceptos básicos, presentaremos las
diferentes definiciones de equivalencia y aproximación observacional, y
finalmente introduciremos la noción de costo que nos permitirá derivar la noción
de mejora buscada.


\subsection{Efectos Computacionales}

Como se mencionó en la introducción introduciremos efectos computacionales
mediante operaciones algebraicas. La operaciones algebraicas se representan como
un conjunto de operadores \(\Sigma\) acompañado de una función de aridad
\(\alpha\), tal que, para cada \(\sigma \in \Sigma, \alpha(\sigma) \in \mathbb{N}\).

Por ejemplo, si se quiere imprimir en pantalla se podría representar como
\(\Sigma = \{ \text{print} \}\), donde \(\alpha(\text{print}) = 1\), ya que
'print' toma un argumento.

A su vez, la mónadas son un mecanismo muy utilizado para modelar efectos
computacionales~\cite{Moggi:Monads}. Para esto diremos que dada una mónada \(T\)
interpreta las operaciones algebraicas \(\Sigma\) si: para cada operador de \(
\sigma \in \Sigma\), \(\sigma\) tenga una interpretación \(\sigma^T\) en \(T\).

El concepto fundamental que nos permitirá introducir las diferentes nociones de
equivalencias modulo efectos es el de \emph{Relator}. Un relator a la mónada
\(T\), denominado \(\Gamma_T\), nos permite trasladar relaciones entre valores
a valores con efectos computacionales.

\begin{definition}[Relator]
  Dada una monada \(T\), \(\Gamma_T\) es un relator de \(T\) si
  para cada relación \(R \subseteq X \times Y\), \(\Gamma_T R \subseteq T X
  \times T Y\), cumpliendo con los siguientes requerimientos.

  \begin{align*}
    (=_{FX}) &\subseteq \Gamma_T (=_X) \\
    \Gamma_T R \circ \Gamma_T S &\subseteq \Gamma_T (R \circ S) \\
    \Gamma_T ((f \times g)^{-1} R) &\subseteq (F f \times F g)^{-1}\Gamma_T R \\
    R \subseteq S &\implies \Gamma_T R \subseteq \Gamma_T S
  \end{align*}

  Y además extiende de manera correcta las operaciones básicas de las mónadas.
  \begin{align*}
    \forall x \in X, y \in Y, \mkRel{x}{R}{y}
         & \implies \mkRel{\eta_X(x)}{\Gamma_T R}{\eta_Y(y)} \\
    \forall  u \in T X, v \in T Y, \mkRel{u}{\Gamma_T R}{v}
         & \implies \mkRel{f^{T}(u)}{\Gamma_T S}{g^{T}(v)}, \\
         & \text{dado que } \forall x \in X, y \in Y, \mkRel{x}{R}{y} \implies
           \mkRel{f(x)}{\Gamma_T S}{g(y)}
  \end{align*}
  \end{definition}

\subsection{Programas con Efectos}

Presentamos un lenguaje que nos servirá de vehículo para escribir programas que
utilicen operaciones algebraicas y que eventualmente puedan evaluarse dentro de
mónadas que interpreten dichas operaciones. El lenguaje a utilizar es un
heredero del Lambda Calculus, donde asumimos un conjunto infinito de variables,
a las cuales anotaremos en general como \(x,y,z\), \(\sigma \in \Sigma\):

\begin{align*}
  M,N \; & ::= M \; to \; x \; . \; N \; | V \; W \;
   |  \; \sigma (M_1 , M_2, \ldots, M_{\alpha{\sigma}})\\
  V,W \; & ::= \lambda \; x \; . \; M \; | \; x
\end{align*}

Dado que se presentan dos categorías sintácticas, definiremos relaciones entre
programas como relaciones dobles en términos y valores. Notaremos a \(\Delta\)
como el conjunto de términos, y \(\Lambda\) el conjunto de valores.

De esta manera, dado operaciones algebraicas \(\Sigma\), una mónada \(T\)
podemos definir una relación de evaluación \( [[ - ]] : \Delta \to T \Lambda \).
Notemos que dado que estamos en presencia de efectos, la evaluación de un
termino \(t\), \([[t]]\) nos devuelve un valor anotado con posibles efectos
computaciones en \(T\), \([[t]] \in T \Lambda\).

Notar que ya nos encontramos en un momento donde podremos definir la noción de
equivalencia y aproximación observacional de programas. Como mencionábamos en la
introducción lo único que necesitamos en una relación entre términos y valores.
Pero la evaluación de términos no nos devuelve valores, sino valores con efectos
dentro de una mónada. Para solucionar este punto se introduce entonces el
concepto de relator.

\begin{definition}[Aproximación Observacional con Efectos]
  Sea \(t,p \in \Delta\), un conjunto de operaciones algebraicas \(\Sigma\), una
mónada que las interprete \(T\) y su relator \(\Gamma_T\),diremos que \(t\)
aproxima a \(p\), \(t \trianglelefteq p\) si y sólo si para todo contexto
\(\mathbb{C}\), \(\mkRel{[[ \mathbb{C}[t] ]]}{\Gamma_T U}{[[ \mathbb{C}[p]
]]}\).

  Donde \(U = \Lambda \time \Lambda\), es decir, la relación que relaciona todos
  los valores entre sí.
  \end{definition}

Notar que la definición nos pide simplemente que los efectos computacionales que
se produjeron durante la evaluación del termino estén relacionados por el
relator \(\Gamma_T\) sin importar los valores directamente. De esta manera se
interpreta la definición que se introdujo en la introducción donde el único
efecto utilizado era la posible no terminación de los programas.

En la siguiente sección veremos como introducir la noción de costos de programas
a la definición de aproximación observacional.

\section{Resultados}

En esta sección veremos como introducir la noción de costos de programas como
un refinamiento de la noción presentada como aproximación observacional. Veremos
de forma intuitiva como lograr introducir una operación que llamaremos \(\tick\)
que nos permitirá indicar cómo y dónde introducir costos de programas.

\subsection{Costos como un Efecto}

Debido que los efectos algebraicos se modelan con una mónada que los interprete,
introduciremos un mecanismo denominado \emph{Transformador Mónadico
Escritor}~\cite{Liang:MTrans}.

Un transformador mónadico \(\Phi\), es un mecanismo que toma como argumento una
mónada \(T\), y nos entrega una nueva mónada, \(\Phi T\) cápale de interpretar
las mismas operaciones que la mónada \(T\) más ciertos nuevos efectos computacionales.

En nuestro caso, presentaremos simplemente el transformador mónadico escritor,
que nos permitirá introducir un contador dentro de la evaluación de
computaciones. Éste contador nos permitirá llevar cuenta del costo que nos
generó evaluar un termino.

\begin{definition}[Transformador Mónadico Escritor]
  Sea \(T\) una mónada, definimos a \(\Phi T (X) \triangleq T(X \times
  \mathbb{N})\). Donde se definen las siguientes operaciones básicas mónadicas.
  \begin{align*}
    \eta_{\Phi T} & : X \to T (X \times \mathbb{N}) \\
    \eta_{\Phi T}(x) &= \eta_T(x, 0) \\
    & \\
    f^{\Phi T} & T (X \times \mathbb{N}) \to T (Y \times \mathbb{N}) \\
    f^{\Phi T}(u) & = g^{T}(u)\\
    & g : X \times \mathbb{N} \to T (Y \times \mathbb{N}) \\
    & g(x,n) = (m(n))^{T}(f(x)) \\
    & m : \mathbb{N} \to (Y \times \mathbb{N} \to T (Y \times \mathbb{N})) \\
    & m(m)(y,n) = \eta_T((y, m + n))\\
  \end{align*}
  Donde  \(f : X \to T(Y \times \mathbb{N})\).
  \end{definition}

El transformador mónadico nos permite introducir la noción de costos a la
evaluación de términos utilizando una operación de \emph{tick}.

\begin{definition}[Tick]
  Dada una mónada \(T\) al aplicarle el transformador mónadico escritor, podemos
  definir la siguiente función de tick.
  \begin{align*}
    \tick[] & : \Phi T X -> \Phi T X \\
    \tick[] (m) &= t^{T}(m) \\
                & \text{donde } t(v,c) = \eta_t(v,c+1)\\
  \end{align*}
  \end{definition}

Lo que nos permite la operación \(\tick\) es introducir una forma de contar el
costo de una computación, simplemente indicando \emph{cuando} se debe contar.

Finalmente, debemos definir un nuevo relator, que dado un relator \(\Gamma_T\)
para la mónada \(T\), nos defina un relator \(\Gamma_{TC}\) para la mónada
\(\Phi T\). Esto lo obtenemos simplemente componiendo con un relator que además
de comparar valores compare el costo necesario para obtenerlos.

\begin{definition}[Relator de Costos]
  Dada una relación \(R \subseteq X \times Y\), podemos definir \(R_c \subseteq
  (X \times \mathbb{N}) \times (Y \times \mathbb{N})\) simplemente como el
  producto de las relaciones \(R\) y \(\geq\). Es decir: \(R_c \triangleq R
  \otimes (\geq)\).
  \end{definition}

Dado un conjunto de operaciones algebraicas \(\Sigma\), con una mónada \(T\) y
su relator \(\Gamma_T\), podemos obtener un modelo computacional que tiene
en cuenta el costo de la ejecución: definimos un nuevo conjunto de operaciones
algebraicas \(\Sigma_{\tick} = \Sigma \cup {\tick}\), donde \(\alpha(\tick) =
1\), con la mónada \(\Phi T\) y relator \(\Gamma_{TC}\).
Utilizando entonces la nueva mónada podemos definir un nuevo modelo de
evaluación donde se indican con \(\tick\) el costo de las diferentes
operaciones. A éste nuevo modelo de evaluación lo notaremos como \([[ - ]]_c\)

De esta manera obtenemos un definición de mejoras simplemente como la
aproximación observacional \textbf{usando el transformador mónadico escritor.}

\begin{definition}[Mejora]
  Sea \(t,p \in \Delta\), un conjunto de operaciones algebraicas \(\Sigma\), una
  mónada que las interprete \(T\) y su relator \(\Gamma_T\).
  Diremos que \(t\) es mejorado por \(p\), \(t \geq p\) si y sólo si para todo contexto
\(\mathbb{C}\), \(\mkRel{[[ \mathbb{C}[t] ]]_c}{\Gamma_{TC} U}{[[ \mathbb{C}[p]
]]_c}\).

  Donde \(U = \Lambda \time \Lambda\), es decir, la relación que relaciona todos
  los valores entre sí.
  \end{definition}

De esta manera se obtiene una noción concreta, refinada de la aproximación
observacional antes definida mediante el uso de composición de relators y
transformadores mónadicos.

A modo de ejemplo, podemos retomar el ejemplo anterior de la suma. Teniendo en
cuenta que ahora debemos anotar el programa con el costo de realizar la suma.
Sea \(M^C_f \mapsto \tick[] \text{print(``Hola'')} \; ; \tick[] 1 + 1\), y 
\(N_f \mapsto \tick[] \text{print(``Hola'')}\; ; 2\).


\section{Conclusiones}

Partiendo de una noción simple de equivalencia y aproximación de programas con
efectos algebraicos, se observó como desarrollar un modelo computacional. Dicho
modelo permite interpretar programas trasladando relaciones entre valores a
programas complejos mediante los relators. Los relator cumplen un rol
fundamental en todo el desarrollo, lo que nos permite luego introducir de una
forma muy simple la noción de costos de programas, y más aún, nos permiten poder
compararlos libremente.

Introducimos la maquinaria necesaria para poder obtener un modelo
computacional que nos permita evaluar programas anotados con costos, y un
relator adecuado que, de forma sencilla, nos permita comparar sus costos.
Cumpliendo finalmente la promesa de interpretar las mejoras de programas como un
refinamiento de la noción de equivalencia y aproximación observacional de programas.

Se pueden obtener las nociones de mejoras de programas clásicas simplemente
restringiendo los efectos algebraicos y utilizando la mónada de
\emph{Terminación}. A su vez, se pueden obtener nuevas nociones de costos sobre
modelos computacionales como ser en presencia de operadores probabilísticos,
no-determinismo, etc. Incluso dentro de los modelos clásicos nos permite obtener
un modelo unificado, las mejoras es re-interpretar los programas usando un
transformador mónadico y un relator de costos.

Debido al enfoque y objetivo del presente documento se han tomado ciertas
licencias matemáticas, y opté por dejar la formalidad de lado e intentar
transmitir las ideas fundamentales. A su vez el trabajo se puede extender
introduciendo mecanismos de pruebas (lemas y teoremas) que permitan simplificar
las pruebas de mejoras, estudiando en concreto la noción de \emph{simulación}.
La simulación de programas permiten reducir los contextos necesarios para
demostrar mejoras de programas.

%%%%%%%%%%%%%%%%%%%%
%% Bib?
\printbibliography
\end{document}
