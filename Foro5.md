Buenas!

En mi opinión involucrar a los actores sociales principalmente y la a sociedad
en general en el desarrollo de la ciencia es vital para el desarrollo de la
misma. Permite mantener acotado el desarrollo de la ciencia por la ciencia
misma, permitiendo tener una conexión mucho más fuerte entre los problemas de la
sociedad y las soluciones de la ciencia. Claro que esto responde una visión
claramente utilitarista de la ciencia. A su vez obliga al científico a buscar el
aval de la sociedad, forzándolo a cumplir con tareas de divulgación y extensión.

De todas maneras la forma en que veo posible la interacción entre la sociedad y
el desarrollo científico no es de un tinte tan directo como se plantea en la
lectura. Particularmente me gustaría pensar en la sociedad como un guía en el
desarrollo y no como un juez, que exige ciertos avances y desarrollos pero no
los controla directamente.

Similar al caso de Paula Cardone, mis desarrollos están un poco lejos de tener
un impacto real sobre la sociedad. Presentando un problema con los párrafos
anteriores, mi investigación no responde directamente a la sociedad ni es guiada
por ella. Incluso dentro de la comunidad científica mis desarrollos son de los
más teóricos y el impacto lo podré observar recién cuando se terminen los
estudios.

De todas maneras siempre traté de involucrarme en trabajos de extensión o de
divulgación y de hacer llegar hasta donde pude ciertos problemas concretos y
soluciones propuestas por mi tesis doctoral. Recuerdo que en ciertas épocas del
año nos visitaban estudiantes de secundaria, pero no se volvió a hacer más (en
mi instituto al menos). Y es en base a estas experiencias que comunicar,
divulgar, trasladar desarrollos altamente teóricos a personas sin conocimiento
especifico ayuda a entender en mayor profundidad los temas que uno mismo está
formando/descubriendo/elaborando.
