Síntesis de proyecto de tesis.
========================================

+ Problema de Investigación / Hipótesis: 
Es posible establecer una relación *co-algebraica* de costos entre programas de carácter general
que permita esbozar técnicas generales para probar optimizaciones en un amplio
espectro de lenguajes que acepten una descripción *algebraica*.

+ Objetivos:
La construcción de un entramado conceptual matemático que permita la elaboración
directa de pruebas de optimizaciones de la forma más simple posible.
Se espera de esta manera avanzar en el conocimiento de las técnicas utilizadas
como a su vez desarrollar nuevas. Presentar un estudio más completo sobre los
tipos de lenguajes sobre el cual dicha técnica es aplicable.

+ Estrategias metodológicas: Estudio de Tipo *explorativo*. Se encuentra
bibliografía anterior a éste trabajo, pero se aborda desde otro punto de vista
desconectado de otras teorías.

+ Enfoque/Diseño Metodológico: No sé encontró dentro del material propuesto que
diseño metodológico utilizar. En principio no es directamente un enfoque
experimental, aunque sería el más indicado ya que en la realidad se utilizó
(lamentablemente) un enfoque de prueba y error. Pero, además, tampoco resulta
ser un diseño no experimental, ya que ambos enfoques toman el tiempo como algo
central, y en realidad en mi trabajo de investigación, como en las matemáticas,
el tiempo no existe.

+ Comentarios/Observaciones: Analizando el trabajo realizado y el material
propuesto, me resultó difícil encasillar mi trabajo. Probablemente sea el hecho
de ser un desarrollo puro matemático, aunque se alguna manera se desarrolla con
una estrategia exploratoria. Lamentablemente fue así la única opción que tuve,
mi trabajo no comenzó por la necesidad de mi supervisor de algún proyecto o
propuesta por él, sino que fue la búsqueda personal de que problema inventarme
para resolver.

Análisis Reflexivo: La articulación marco teórico - objetivos/hipótesis - metodología
================================================================================

Críticamente sobre el trabajo presentado en el plan original de mi propuesta
doctoral, no sé tuvieron en cuenta ninguno de los puntos detallados en el
transcurso del curso. Se revisó el marco teórico del desarrollo que en su
momento parecía plausible, aunque luego de haber mostrado su falsedad, se
encontraron desarrollos científicos donde brindaban evidencia que no era
factible tal tesis.

Me resulta complicado trabajar sobre el marco teórico de mí tesis actual, ya que
todavía no fue realizado. La articulación con los puntos planteados en el
trabajo anterior, objetivo e hipótesis, y la metodología planteada en éste
trabajo, se observa una estrecha relación entre lo planteado y el qué hacer
diario. La metodología explorativa describe directamente lo que fue el
desarrollo de mi tesis, y en particular la aclaración que *rara vez presentan
una respuesta*. Lo único que no pude relacionar fue el diseño metodológico, ya
que no presenta un diseño experimental, donde se busca una causalidad entre
variables por medio de la experimentación y observación, y las diseños no
experimentales propuestos incluyen directamente una noción temporal que no es
parte de esta investigación.
